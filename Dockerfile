# Start by building the application.
FROM golang:1.12 as build-env

ENV GOSS_VER v0.3.7

WORKDIR /go/src/app

# (optional) dgoss docker wrapper (use 'master' for latest version)
RUN curl -L https://raw.githubusercontent.com/aelsabbahy/goss/master/extras/dgoss/dgoss -o /go/src/app/dgoss \
&& chmod +rx /go/src/app/dgoss

FROM gcr.io/distroless/base
COPY --from=build-env /go/src/app/* /
